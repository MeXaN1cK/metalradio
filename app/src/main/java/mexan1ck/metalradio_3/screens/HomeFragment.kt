package mexan1ck.metalradio_3.screens

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import android.widget.ToggleButton
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import mexan1ck.metalradio_3.R
import mexan1ck.metalradio_3.R.drawable.ic_volume_off
import mexan1ck.metalradio_3.R.drawable.ic_mute
import mexan1ck.metalradio_3.api.RadioService
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class HomeFragment : Fragment(), View.OnClickListener {
    private lateinit var mView: View
    private var btn_play: ImageButton? = null
    private var btn_stop: ImageButton? = null
    private var btn_mute: ToggleButton? = null
    private var mTextView: TextView? = null
    private lateinit var radioService: RadioService
    private var mBound: Boolean = false
    private var text = ""

    private val connection = object : ServiceConnection {

        override fun onServiceConnected(className: ComponentName, service: IBinder) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            val binder = service as RadioService.MyBinder
            radioService = binder.getService()
            mBound = true
            Log.i("###", "bind");
            radioService.artist_title.observe(viewLifecycleOwner, Observer {
                text = if (it.size > 2) {
                    "${it[0]} - ${it.subList(1, it.lastIndex + 1).joinToString(separator = "-")}"
                } else {
                    "${it[0]} - ${it[1]}"
                }
                //Log.d("LIVEDATA", text)
                doAsync {
                    uiThread {
                        mTextView?.text = text
                        mTextView?.refreshDrawableState()
                    }
                }
            })
        }

        override fun onServiceDisconnected(arg0: ComponentName) {
            mBound = false
            Log.i("###", "unbind");
        }
    }

    companion object {
        private const val ARG_NAME = "ARGS"
        fun getInstance() : HomeFragment {
            return HomeFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState != null) {
            Log.d("STATE", savedInstanceState.toString())
            btn_play?.isClickable = savedInstanceState.getBoolean("btn_play_isClickable")
            btn_play?.isVisible = savedInstanceState.getBoolean("btn_play_isVisible")
        }
        doAsync {
            uiThread {
                mTextView?.refreshDrawableState()
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_home, container, false)
        btn_play = mView.findViewById(R.id.btn_play)
        btn_stop = mView.findViewById(R.id.btn_stop)
        btn_mute = mView.findViewById(R.id.btn_mute)
        mTextView = mView.findViewById(R.id.artist_title)
        radioService = RadioService()
        doAsync {
            uiThread {
                mTextView?.refreshDrawableState()
            }
        }
        return mView
    }

    override fun onStart() {
        super.onStart()
        btn_play?.setOnClickListener(this)
        btn_stop?.setOnClickListener(this)
        btn_mute?.setOnClickListener(this)
        doAsync {
            uiThread {
                mTextView?.refreshDrawableState()
            }
        }
    }

    override fun onStop() {
        super.onStop()
    }

    override fun onResume() {
        doAsync {
            uiThread {
                mTextView?.refreshDrawableState()
            }
        }
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        btn_play?.isClickable?.let { outState.putBoolean("btn_play_isClickable", it) }
        btn_play?.isVisible?.let { outState.putBoolean("btn_play_isVisible", it) }
        super.onSaveInstanceState(outState)
    }
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_play -> {
                btn_play?.isClickable = false
                btn_play?.isVisible = false
                btn_stop?.isClickable = true
                Log.i("###", "play " + activity)
                activity?.startService(Intent(activity, RadioService::class.java))
                activity?.bindService(
                    Intent(activity, RadioService::class.java),
                    connection,
                    Context.BIND_AUTO_CREATE
                )
                doAsync {
                    uiThread {
                        mTextView?.refreshDrawableState()
                    }
                }
            }
            R.id.btn_stop -> {
                btn_play?.isClickable = true
                btn_play?.isVisible = true
                btn_stop?.isClickable = false
                Log.i("###", "stop")
                activity?.unbindService(connection)
                activity?.stopService(Intent(activity, RadioService::class.java))
            }
            R.id.btn_mute -> {
                if (mBound) {
                    when (btn_mute!!.isChecked) {
                        true -> {
                            radioService.volumeHandler(true)
                            btn_mute!!.foreground = ResourcesCompat.getDrawable(
                                resources,
                                ic_volume_off,
                                resources.newTheme()
                            )
                        }
                        false -> {
                            radioService.volumeHandler(false)
                            btn_mute!!.foreground = ResourcesCompat.getDrawable(
                                resources,
                                ic_mute,
                                resources.newTheme()
                            )
                        }
                    }
                }
            }
        }
    }
}