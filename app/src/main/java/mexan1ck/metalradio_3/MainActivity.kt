package mexan1ck.metalradio_3

import android.content.res.Configuration
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.ActionBar
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.viewpager.widget.ViewPager
import com.ismaeldivita.chipnavigation.ChipNavigationBar
import kotlinx.android.synthetic.main.activity_main.*
import mexan1ck.metalradio_3.api.ViewPagerAdapter
import mexan1ck.metalradio_3.screens.HomeFragment
import mexan1ck.metalradio_3.screens.SettingsFragment

class MainActivity : AppCompatActivity() {

    lateinit var chipNavigationBar: ChipNavigationBar
    lateinit var fragmentManager: FragmentManager
    lateinit var fragment: Fragment
    lateinit var mViewPager: ViewPager
    lateinit var mViewPagerAdapter: ViewPagerAdapter
    lateinit var mActionBar: ActionBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mActionBar = supportActionBar!!
        chipNavigationBar = findViewById(R.id.bottom_nav)

        if (savedInstanceState != null) {
            fragment = HomeFragment.getInstance()
            fragmentManager.beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .commit()
        }
        if (supportFragmentManager.findFragmentById(R.id.home_fragment) !== null) {
            fragment = supportFragmentManager.findFragmentById(R.id.home_fragment)!!
        }
        else {
            chipNavigationBar.setItemSelected(R.id.home, true)
            fragmentManager = supportFragmentManager
            fragment = HomeFragment()
            fragmentManager.beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .commit()
        }

        mViewPagerAdapter = ViewPagerAdapter(supportFragmentManager)
        mViewPager = fragment_container
        setupViewPager(mViewPager)

        chipNavigationBar.setOnItemSelectedListener(listener = object :
            ChipNavigationBar.OnItemSelectedListener {
            override fun onItemSelected(id: Int) {
                when (id) {
                    R.id.home -> {
                        setViewPager(0)
                        chipNavigationBar.setItemSelected(id, true)
                    }
                    R.id.settings -> {
                        setViewPager(1)
                        chipNavigationBar.setItemSelected(id, true)
                    }
                }
            }
        })
    }

    private fun setupViewPager(viewPager: ViewPager) {
        val adapter = ViewPagerAdapter(supportFragmentManager)
        adapter.addFragment(HomeFragment(), "Home")
        adapter.addFragment(SettingsFragment(), "Settings")
        viewPager.adapter = adapter
    }

    fun setViewPager(fragmentNumber: Int) {
        mViewPager.currentItem = fragmentNumber
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            mActionBar.hide()
        } else {
            mActionBar.show()
        }
    }
}