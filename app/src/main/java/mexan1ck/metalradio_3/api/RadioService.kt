package mexan1ck.metalradio_3.api

import android.app.*
import android.content.Context
import android.content.Intent
import android.media.AudioManager
import android.media.AudioManager.STREAM_MUSIC
import android.media.MediaPlayer
import android.os.Binder
import android.os.Build
import android.os.IBinder
import android.os.PowerManager
import android.util.Log
import androidx.annotation.Nullable
import androidx.core.net.toUri
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import mexan1ck.metalradio_3.MainActivity
import mexan1ck.metalradio_3.R
import mexan1ck.metalradio_3.screens.HomeFragment
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.notificationManager
import java.util.*

const val ID = 666
const val TAG = "Notification"
const val NOTIFICATION_CHANNEL_ID = "notification"
const val NOTIFICATION_CHANNEL_NAME = "Radio Notification"

class RadioService : Service(), LifecycleOwner, LifecycleObserver, MediaPlayer.OnPreparedListener {

    private lateinit var mediaPlayer: MediaPlayer
    private lateinit var audioManager: AudioManager
    private val audioUrl = "http://209.95.50.189:8066/stream"
    private val api = SimpleApi()
    private val mBinder: Binder = MyBinder()
    var artist_title: MutableLiveData<List<String>> = MutableLiveData()
    var currentVolume: Int = 0
    private lateinit var manager: NotificationManager

    override fun onCreate() {
        audioManager = getSystemService(Context.AUDIO_SERVICE) as AudioManager
        mediaPlayer = MediaPlayer().apply {
            setOnPreparedListener(this@RadioService)
        }
    }

    override fun onStart(intent: Intent?, startId: Int) {
        currentVolume = audioManager.getStreamVolume(STREAM_MUSIC)
        mediaPlayer.apply {
            setDataSource(applicationContext, audioUrl.toUri())
            prepareAsync()
            setWakeMode(applicationContext, PowerManager.PARTIAL_WAKE_LOCK)
        }
        try {
            mediaPlayer.start()
            doAsync {
                val timer = Timer()
                timer.schedule(object : TimerTask() {
                    override fun run() {
                        try {
                            val response = api.run("http://209.95.50.189:8066/currentsong?sid=1")
                            Log.d("HTTP", response)
                            artist_title.postValue(response.split(" - "))
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                }, 0, 5000)
            }
        } catch (e: Exception) {
            Log.d("EXCEPTION ", e.toString())
        }
        startForeground(ID,createNotification())
    }

    override fun onDestroy() {
        mediaPlayer.stop()
    }

    inner class MyBinder : Binder() {
        fun getService(): RadioService = this@RadioService
    }

    @Nullable
    override fun onBind(intent: Intent?): IBinder {
        return mBinder
    }

    override fun onPrepared(mp: MediaPlayer?) {
        mp?.start()
    }

    override fun getLifecycle(): Lifecycle {
        return lifecycle
    }

    fun volumeHandler(state: Boolean) {
        if (state) {
            audioManager.setStreamVolume(STREAM_MUSIC, 0, 0)
        } else {
            audioManager.setStreamVolume(STREAM_MUSIC, currentVolume, 0)
        }
    }

    private fun getManager(): NotificationManager {
        manager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        return manager
    }

    fun createNotification(): Notification? {
        val notificationIntent = Intent(applicationContext, HomeFragment::class.java)
        val pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0)
        val builder: Notification.Builder = Notification.Builder(this)
            .setContentTitle("Title")
            .setContentText("TEXT")
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            .setContentIntent(pendingIntent)
        return when {
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.O -> {
                val channel = NotificationChannel(
                    NOTIFICATION_CHANNEL_ID,
                    NOTIFICATION_CHANNEL_NAME,
                    NotificationManager.IMPORTANCE_LOW
                )
                getManager().createNotificationChannel(channel)
                builder.setChannelId(NOTIFICATION_CHANNEL_ID).build()
            }
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN -> {
                builder.build()
            }
            else -> {
                builder.getNotification()
            }
        }
    }
}